<%-- 
    Document   : login
    Created on : 26/09/2015, 19:58:06
    Author     : Lucas Chociay <lucasc@alunos.utfpr.edu.br>
--%>
<%@page import="java.util.*" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<html xmlns="http://www.w3.org/1999/xhtml">
   <head>
      <title>Login</title>
   </head>
   <body>
      <form method="post" action="/pratica-jsp/login.jsp">
         Código: <input type="text" name="login"/><br/>
         Nome: <input type="password" name="senha"/><br/>
         Perfil: <select name="perfil">
                     <option value="1">Cliente</option>
                     <option value="2">Gerente</option>
                     <option value="3">Administrador</option>
                 <select>
         <input type="submit" value="Enviar"/>
      </form>
   </body>
</html>
       
       <jsp:useBean id="loginBean"
                    class="pratica.jsp.loginBean">
           <jsp:setProperty name ="loginBean" property="*"/>
       </jsp:useBean>
               
       <%
            
           
            String login = loginBean.getLogin();
            String senha = loginBean.getSenha();
            String perfil = loginBean.getPerfil();
            Date data = new Date();
            
            if(perfil != null){
                switch(perfil){
                    case "1":
                        perfil = "Cliente";
                        break;
                    case "2":
                        perfil = "Gerente";
                        break;
                    case "3":
                        perfil = "Administrador";
                        break;
                }    
            }
            
            if(login != null){
                if(login.equals(senha)){
                    out.println("<div style=\"color:#0000FF\">");
                    out.println(perfil + ", login bem sucedido" + ", para " + login + " às " +data);
                    out.println("</div>");
                }
                else{
                    out.println("<div style=\"color:#ff0000; font-style:italic\">");
                    out.println("Acesso Negado");
                    out.println("</div>");
                }
            }
           %>
           
   

